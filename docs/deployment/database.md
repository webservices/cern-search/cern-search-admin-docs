# Relational Database

New clients/instances need a new database. Each time that a new instance is deployed we need to create a new user and a new database for it.

To create the new user and new database follow these steps:

1. Login as admin in the DBoD instance: 
```bash
$ psql -h <HOST> -p <PORT> -U <ADMIN_USER>
```
2. Create the database (with the name of the client, e.g. webservices, edms): 
```sql
CREATE DATABASE <DB_NAME>;
```
3. Create the user with the same name as the database: 
```sql
CREATE USER <USER_NAME> WITH PASSWORD '<PASSWORD>';
```
4. Grant the necessary priviledges to the user: 

```sql
GRANT ALL PRIVILEGES ON DATABASE <DB_NAME> to <USER_NAME>;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO <USER_NAME>;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO <USER_NAME>;
```
5. Configure the ``pg_hba.conf`` file to allow the new user to connect.
```bash
# User related -- PLEASE EDIT HERE
host      all          admin      0.0.0.0/0   md5
host  <DB_NAME>  	<USER_NAME>   0.0.0.0/0   md5
host    postgres    <USER_NAME>   0.0.0.0/0   md5
```

!!! warning "Reloading"
	You might need to "reaload processes" in order to make the ``pg_hba.conf`` file effective.