# Sentry 

If you have set up a Sentry server, you can configure ``INVENIO_LOGGING_SENTRY_LEVEL`` (As a configuration parameter in ``rest-api/configuration.yml``) and ``INVENIO_SENTRY_DSN`` (As a secret loaded in ``rest-api/application.yml``) and send the logs and traces to it.