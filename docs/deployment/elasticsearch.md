# Elasticsearch

All indexes are stored in an ES instance. In order to provide ACLs using [ReadOnly-REST](https://readonlyrest.com/) an account/user for each instance is created. This user will only have access to certain prefixes, which will be ``<PRFIX>_<INSTANCE_NAME>``.

For example, the indexes ``cernsearch-test_doc_v1.0.0`` and ``cernsearch-test_collection_v1.0.0`` will be access by the user ``cernsearch-test``, who will only have access to ``cernsearch-test[_-]*`` indexes.

!!! tip "Production vs QA"
	The logic is the same. You just need to change the prefix of your indexes, for example ``cernsearch`` could be Prod and ``cernsearchqa`` QA. 
