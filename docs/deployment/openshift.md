# OpenShift

## CERN Search Template

You need to import the appropriate yaml files (``rest-api`` for the RESTful API and ``web-ui`` for the web user interface) form the template. These are located in [cern-search-openshift](https://gitlab.cern.ch/webservices/cern-search/cern-search-openshift). The file names are self explanatory.

First import the ``configuration.yml`` file and fill in the corresponding values to match those of the new instance. A sample configuration ``sample-production.params`` which allows 100 simultaneous multiple connections can be observed. Note that files indexing leads to 100 more connections to the DB.
Import ``secrets`` as well and fill the base64 encoded values or alternatively created them manually and they will be encoded automatically.

Afterwards do the same, starting with ``storage.yml`` and then ``services.yml``, ``application.yml`` and ``routes.yml``, following this order. The instance should be ready as soon as the pods have been started.

!!! warning "First deployment of an instance"
	The first time an instance is deployed the ``scripts/populate-instance.sh`` has to be run manually. For this access the terminal in a ``web`` POD and run ``sh populate-instance.sh``. This will create the database and its table in the DBoD instance, as well as the appropriate indexes in ES.

This instructions assume that the image stream (available in ``image-stream.yml``) is set up in the master project and all its children inherit the image from it. 

If it is not the case, you should also import the ``image-stream.yml`` file, and set to the corresponding values ``APPLICATION_IMAGE_NAME``, ``APPLICATION_IMAGE_TAG`` and ``TAGS_PROJECT``. Finally if you wish to push the image when gitlab's pipelines are successfully finished, perform the steps shown in the following section 

!!! tip "Not needed if using the master project"
	This is not needed if you are using the image from the master/parent project mentioned above.

## Automate image deployment via GitLab-CI

1. Make project public in gitlab
2. Create a service account in the openshift project:
	- oc project <project name\>
	- oc create serviceaccount <sa name\>
3. Get the value of one of the tokens:
	- oc get secrets
	- oc describe secret <sa name\>-token-xxxxx
4. Create a secret in gitlab:
	- Gitlab -> CI/CD Settings -> Secret variables
	- Add SERVICE_ACCOUNT_TOKEN_<DEV|PROD\>
5. Give the account the rights to edit the tag in openshift
	- oc adm policy add-role-to-user edit -z <sa name\>

## Readiness and health checks

In order to provide automatic scaling and monitoring capabilities, 3 health checks have been implemented:

- ``health/uwsgi`` answers a ``200: OK`` if the web app is up and running.
- ``health/elasticsearch`` answers a ``200: OK`` if the web app can connect to Elasticsearch (performs a ``client.ping()`` operation).
- ``health/database`` answers with a ``200: OK`` if the web app can perform a raw select operation ``SELECT 1;`` through the db connection.

A readiness and liveliness probe has been set in OpenShift.
