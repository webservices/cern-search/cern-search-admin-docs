# Quickstart

To deploy a new instance we provide the openshift templates used by our production instances. 
We provide as well a docker-compose recipe not optimized for production. 
If using the recipe in docker-compose be aware replication and scalability was not taken into account.
SSL certificates generation must be handled by the admin. 
