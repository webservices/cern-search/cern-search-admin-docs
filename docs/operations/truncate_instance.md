# Truncate Instance


## 1. Database (PostgreSQL)
Start by truncate database (except users and auth data).

### Connect via terminal

```bash
psql -h <HOST>.cern.ch -p <PORT> -W <DATABASE> -U <USER>
```

A prompt will appear asking for the password of that user to that database.

### Wipe all records from database


```bash
TRUNCATE records_metadata_version;
TRUNCATE records_metadata, records_buckets;
TRUNCATE pidstore_redirect;
TRUNCATE pidstore_pid CASCADE;
TRUNCATE pidstore_recid;
TRUNCATE transaction;
TRUNCATE files_bucket, files_files, files_object CASCADE;
```

Check that there is no remaining data:

```bash
SELECT COUNT(*) FROM files_bucket;
SELECT COUNT(*) FROM files_files;
SELECT COUNT(*) FROM files_object;
SELECT COUNT(*) FROM records_metadata;
SELECT COUNT(*) FROM records_buckets;
SELECT COUNT(*) FROM records_metadata_version;
SELECT COUNT(*) FROM pidstore_redirect;
SELECT COUNT(*) FROM pidstore_pid;
SELECT COUNT(*) FROM pidstore_recid;
SELECT COUNT(*) FROM transaction;
```

## 2. Clean permanent storage.
Enter a pod and remove all files under `DEFAULT_RECORDS_FILES_LOCATION`.
``` bash
$ rm -rf <DEFAULT_RECORDS_FILES_LOCATION>/*
```

## 3. Finish with recreating ES indeces. 
Enter a pod and run
``` bash
$ bash -c "invenio index destroy"
$ bash -c "invenio index init"
```

