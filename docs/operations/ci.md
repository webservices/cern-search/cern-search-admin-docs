# CI/CD 

Linting and testing have been set up to run on every merge request.
Testing is mandatory to pass, however linting is optional because it has been introduced later in the project. 
Changed files should pass linting, and for that purpose it is recommended to install locally [pre-commit](https://pre-commit.com/#install) which is already configured.

When linting state is improved sufficiently it should be made a mandatory step.
Currently linting is made with the tools `isort` and `flake8`.

# Development
Development can be made through 2 environments: one fully powered with `docker-compose.test.yml` and another through pipenv running locally the app and the remaining services running with `docker-compose.yml`.
A makefile to ease processes exist with very useful commands.

For example to boot up the fully docker development environment run:

```bash
$ make env MODE=test
```
The flag `MODE=test` enables root permissions and mapping to the locally installed egg to allow fully functional features during local development.

The file `docker-compose.full.yml` serves as an example for semi-production environment in a docker environment.

And the pipenv mode:

```bash
$ make local-env
```

Some other useful commands are:
```bash
# lint
$ make lint MODE=test
$ make local-lint

# test
$ make test MODE=test
$ make local-test

# shell env
$ make shell-env MODE=test
$ make shell-worker MODE=test
$ make shell-local-env

# logs
$ make logs MODE=test
$ make local-env-logs
```

For the full list read the `Makefile`.
