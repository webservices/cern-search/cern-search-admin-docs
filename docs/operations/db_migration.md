# Database migration

Each time a new table is required due to a new dependency being added for example,
or a new field is added due to a dependency being upgraded a db migration is needed.
When this happens run the following command inside the `web` container:

``` bash
$ bash -c "invenio alembic upgrade"
```
