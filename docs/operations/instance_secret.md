# Update Instance Secret Key

All Invenio applications have a secret key that is used to encrypt and generate session keys and other important tokens. By default its value is *CHANGE_ME*.

Any user that knows this value would be able to generate sessions and potentially be authenticated as admin (i.e. session of user_id ``1``).

In order to update this key follow these steps:

1- Open a terminal (e.g. in an OpenShift POD) and get an Invenio shell to check the current secret value (write it down since it will be needed to update it):

``` bash
$ /bin/bash -c "invenio shell"
> app.config['SECRET_KEY']
```

2- Generate a new secret. For example using the ``openssl`` linux library.

``` bash
openssl rand -hex 8
```

3- Add an environment variable named ``INVENIO_SECRET_KEY`` that mounts the secret that contains the previously generated value (i.e. generate secret, update deployment config and redeploy).

4- On a terminal run

``` bash
/bin/bash -c "invenio instance migrate-secret-key --old-key <OLD_KEY>"
```

Your work is done.
