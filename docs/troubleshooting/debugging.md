# Debugging
CERN Search and its components (crawler, indexer, etc.) are deployed in OpenShift. Nonetheless, there is also a dev instance whose URL is ``dev-cern-search.web.cern.ch``. Queries (GET, POST, PUT, PATCH, DELETE) can be tested there (see GitLab repository to get the test schemas' fields).

## Error logs
Error logs can be found in [Sentry](https://test-search-sentry.web.cern.ch/sentry/), choosing the corresponding team (a.k.a. client) and project. Users have to belong to ``CernSearch-Users`` to be able to login. However, they should be invited to the corresponding team/project.

## Locally
To debug the system locally check [CI/CD](../operations/ci.md).

## Invenio Shell

A useful tool is ``invenio-shell``, which works as a python shell although with all the flask application context (extensions, blueprints, etc.) loaded. For that just execute ``invenio shell`` in your project folder (assuming the environment has been already properly set up).

useful commands:

  - ``app.wsgi_app.mounts['/api'].url_map``: Allows you to get the current URLs loaded in the api app.

## Running with Gunicorn
`` gunicorn --threads 3 -t 300 -b :5000 --certfile=<PATH_TO>/ssl.crt --keyfile=<PATH_TO>//ssl.key cern_search_rest_api.wsgi ``

## Openshift 
Enter pods using the openshift shell.

Monitor workers and queues accessing the routes `https://tasks-<domain>.app.cern.ch` and `https://queues-<domain>.app.cern.ch`.
