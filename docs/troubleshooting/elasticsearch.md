#Elasticsearch

## GET all indeces
```bash
curl -X GET "https://$USER:$PASSWORD@$ESHOST/_cat/indices?v"
```

## GET all aliases
```bash
curl -X GET "https://$USER:$PASSWORD@$ESHOST/_cat/aliases?v"
```
## CREAT alias
```bash
curl -X POST "https://$USER:$PASSWORD@$ESHOST/_aliases" -H 'Content-Type: application/json' -d'
{
    "actions" : [
        { "add" : { "index" : "index_name", "alias" : "alias_name" } }
    ]
}
'
```
## GET mapping
```bash
curl -X GET "https://$USER:$PASSWORD@$ESHOST/$INDEX/_mapping/_doc"
```

## GET all documents in an index
```bash
curl -X GET "https://$USER:$PASSWORD@$ESHOST/$INDEX/_search?pretty=true" -H 'Content-Type: application/json' -d'
{
    "query": {
        "match_all": {}
    }
}
'
```

## DELETE index
```bash
curl -X DELETE "https://$USER:$PASSWORD@$ESHOST/$INDEX"
```

## DELETE a record
```bash
curl -X DELETE "https://$USER:$PASSWORD@$ESHOST/$INDEX/$DOC_TYPE/$UUID"
```

## UPDATE By Query

To prevent schema explosion, is a good practice to avoid dynamic field names. This is because ES will create a new field for each unique name, which will decrease performance and increase the required space.

For example in the case of tuples like (title, content), it would have to be specified as an object of two fields: 

```json
{
    "title": "title_value",
    "content": "content value"
}
```

Instead of one with the value of the title as name and the value of the content as value. 

```json
{
    "title_value": "content value"
}
```

This approach presents a problem when updating, since it is needed to search for the document(s) that have to be updated. For root documents with an ID it would be straight forward: First search the document, then update the corresponding document(s) using json patch syntax. However, for inner objects this cannot be done. Therefore, we need to use [update_by_query](https://www.elastic.co/guide/en/elasticsearch/reference/5.6/docs-update-by-query.html). An example is (note that it is run in local, not in the cluster):

1\. Create a mapping

```bash
 curl -X PUT localhost:9200/cernsearch-ubq-test -d '{"mappings":{ "_default_":{"properties":{"links":{"type":"nested"}}}}}'
```


2\. Add two documents

```bash
curl -X PUT localhost:9200/cernsearch-ubq-test/my_type/1 -d '{"id":1 , "links": [{"title":"t1", "content":"content of the first title"}, {"title":"t2", "content": "content of the second title"}]}'

curl -X PUT localhost:9200/cernsearch-ubq-test/my_type/2 -d '{"id":2 , "links": [{"title":"t1", "content":"content of the first title"}, {"title":"t3", "content": "content of the third title"}]}'
```


3\. Check that the existing documents are correct

```bash
curl -XGET "localhost:9200/cernsearch-ubq-test/_search?pretty=true" -H 'Content-Type: application/json' -d'
{
    "query": {
        "match_all": {}
    }
}
'
```

4\. Create an update script

Note that this script will only work on ES version 5.6.X (or highger, although it has only been tested in 5.6.9).

```bash
{
  "query": {
    "nested": {
      "path": "links",
      "query": {
        "term": {
          "links.title": "t1"
        }
      }
    }
  },
  "script": {
    "source": "for (item in ctx._source.links){if (item.title == 't2'){item.content='Updated content'}}",
    "lang": "painless"
  } 
} 
```

5\. Execute an ``update_by_query`` operation

Assuming the above decribed script has been saved in the ``ubq`` file, the execution of the updated would be:

```bash
curl -X POST localhost:9200/cernsearch-ubq-test/_update_by_query --data-binary @ubq
```

6\. Check that the result was the expected

```bash
curl -XGET "localhost:9200/cernsearch-ubq-test/_search?pretty=true" -H 'Content-Type: application/json' -d'
{
    "query": {
        "match_all": {}
    }
}
'
```
