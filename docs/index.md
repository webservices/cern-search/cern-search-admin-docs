# CERN search documentation
Welcome to the CERN Search administration documentation. Here you will find the  documentation available on how to deploy new instances, perfom daily operations, troubleshoot problems and more.

For documentation on how the platform works and how to interact with it please refer to [CERN Search user documentation](https://cern-search.docs.cern.ch/).

The documentation is divided in the following sections:

- Deployment: In this section we explain the steps you need to take in order to deploy a new instance.
- Operations: This section is dedicated to tasks you might need to perform as part of the daily operations.
- Troubleshooting: If you encountered a problem, you might find the cause documented here.

!!! warning "Versions"
	The current platform supports Python 3.4 or grater and Elasticsearch 6.2.4 or grater. Other versions might work, however we do not provide support for it.