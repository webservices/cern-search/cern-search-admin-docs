site_name: CERN Search Administration Documentation
site_url: https://cern-search-admin.docs.cern.ch/
site_author: CERN Search Administrators

theme:
    name: material
    highlightjs: true
    hljs_style: github
    hljs_languages:
        - yaml

extra_css:
    - css/extra.css

repo_url: 'https://gitlab.cern.ch/webservices/cern-search/cern-search-admin-docs'

markdown_extensions:
    - admonition
    - pymdownx.superfences
    - toc:
        permalink: true

nav:
    - Where to start: index.md
    - Architecture: architecture.md
    - Design your datamodel:
        - Elasticsearch mappings: datamodel/es_mapping.md
    - Deployment:
        - Quickstart: deployment/quickstart.md
        - Configuration: deployment/configuration.md
        - Elasticsearch: deployment/elasticsearch.md
        - SQL Database: deployment/database.md
        - Redis: deployment/redis.md
        - Openshift: deployment/openshift.md
        - Sentry: deployment/sentry.md
    - Operations:
        - CI/CD: operations/ci.md
        - Data migration: operations/data_migration.md
        - Promoted results: operations/promoted_results.md
        - Update instance secret key: operations/instance_secret.md
        - Truncate Instance Data: operations/truncate_instance.md
        - Database migration: operations/db_migration.md
    - Troubleshooting:
        - CERN Search REST API: troubleshooting/cern_search_rest_api.md
        - Debugging in local: troubleshooting/debugging.md
        - Elasticsearch: troubleshooting/elasticsearch.md
        - Containers: troubleshooting/containers.md
plugins:
    - search
