# cern-search-admin-docs

Administration documentation for Citadel Search


## Continuous Deployment

Follow [how-to.docs.cern.ch](https://how-to.docs.cern.ch/new/).


## To run locally

```sh
$ pip install mkdocs mkdocs-material

# if you want to run it locally  a
$ mkdocs serve

# in order to generate HTML files
$ mkdocs build
```